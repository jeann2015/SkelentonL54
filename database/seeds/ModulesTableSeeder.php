<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'description' => 'Principal',
            'order' => '1',
            'id_father' =>'0',
            'url' =>'#',
            'messages' => 'Principal',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Users',
            'order' => '2',
            'id_father' =>'1',
            'url' =>'users',
            'messages' => 'Users',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Audit',
            'order' => '3',
            'id_father' =>'1',
            'url' =>'audit',
            'messages' => 'Audit',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Modules',
            'order' => '4',
            'id_father' =>'1',
            'url' =>'modules',
            'messages' => 'Modules',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);



        DB::table('modules')->insert([
            'description' => 'Reports',
            'order' => '1',
            'id_father' =>'0',
            'url' =>'#',
            'messages' => 'Reports',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Reports Example',
            'order' => '1',
            'id_father' =>'5',
            'url' =>'reportventas',
            'messages' => 'Reports Example',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
