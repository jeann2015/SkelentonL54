<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AccessTableSeeder extends Seeder
{
   /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('access')->insert([
            'id_user' => '1',
            'id_module' => '1',
            'views' =>'1',
            'inserts' =>'1',
            'modifys' =>'1',
            'deletes' =>'1',
            'status' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('access')->insert([
            'id_user' => '1',
            'id_module' => '2',
            'views' =>'1',
            'inserts' =>'1',
            'modifys' =>'1',
            'deletes' =>'1',
            'status' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('access')->insert([
            'id_user' => '1',
            'id_module' => '3',
            'views' =>'1',
            'inserts' =>'1',
            'modifys' =>'1',
            'deletes' =>'1',
            'status' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('access')->insert([
            'id_user' => '1',
            'id_module' => '4',
            'views' =>'1',
            'inserts' =>'1',
            'modifys' =>'1',
            'deletes' =>'1',
            'status' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('access')->insert([
            'id_user' => '1',
            'id_module' => '5',
            'views' =>'1',
            'inserts' =>'1',
            'modifys' =>'1',
            'deletes' =>'1',
            'status' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);


        DB::table('access')->insert([
            'id_user' => '1',
            'id_module' => '6',
            'views' =>'1',
            'inserts' =>'1',
            'modifys' =>'1',
            'deletes' =>'1',
            'status' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
