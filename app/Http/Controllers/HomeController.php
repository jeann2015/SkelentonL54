<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iduser = \Auth::id();   

        $m = new Modules();      

        $module_principals = $m->get_modules_principal_user($iduser);
        
        $module_menus = $m->get_modules_menu_user($iduser);

        return view('home',compact('module_principals','module_menus'));
    }
}
