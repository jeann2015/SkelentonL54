<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Modules;

use App\Audits;

class ModulesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $modules = Modules::all();
        return view('modules.index', compact('modules', 'user_access'));
    }

    public function add(Request $request)
    {
        return view('modules.add');
    }

    public function news(Request $request)
    {
        $audits = new Audits;

        $module = Modules::create([
            'description'=>$request->description,
            'order'=>$request->order,
            'id_father'=>$request->id_father,
            'url'=>$request->url,
            'visible'=>$request->visible,
            'messages'=>$request->messages,
            'status'=>$request->status]);

        $module->agregar_nuevo_modulos_user($module->id);
        $audits->save_audits('Add new Module:'.$module->id." - ".$request->description);
        return redirect('modules');
    }


    public function edit(Request $request)
    {
        $modules = Modules::find($request->id);
        return view('modules.mod', compact('modules'));
    }

    public function update(Request $request)
    {
        $audits = new Audits;

        $module = Modules::find($request->id);
        $module->description = $request->description;
        $module->order = $request->order;
        $module->url = $request->url;
        $module->id_father = $request->id_father;
        $module->visible = $request->visible;
        $module->status = $request->status;
        $module->messages = $request->messages;
        $module->save();
        $audits->save_audits('Modify a Module:'.$request->id." - ".$request->description);
        return redirect('modules');
    }

    public function delete(Request $request)
    {
        $modules = Modules::find($request->id);
        return view('modules.del', compact('modules'));
    }

    public function destroy(Request $request)
    {
        $audits = new Audits;

        $module = Modules::find($request->id);
        $module->eliminar_modulos_user($request->id);
        $module->delete();

        $audits->save_audits('Delete a Module:'.$request->id." - ".$request->description);

        return redirect('modules');
    }
}
