<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Collection as Collection;

use App\Access;

class Modules extends Model
{

	protected $table="modules";

	protected $fillable = [
        'description','order','id_father','url','messages','status','visible'
    ];

	public function agregar_nuevo_modulos_user($idmodule) {    
		$users = User::all();    
        foreach ($users as $user ) {
            $access = new Access; 
            $access->id_user=$user->id;
            $access->id_module=$idmodule;
            $access->views=0;
            $access->inserts=0;
            $access->modifys=0;
            $access->deletes=0;
            $access->status=1;
            $access->save();    
            
        }

	}

	public function eliminar_modulos_user($idmodule) {    
		$users = User::all();    
        foreach ($users as $user ) {
            
            $access = \DB::table('access')
            ->where('access.id_module', '=',$idmodule)
            ->where('access.id_user','=',$user->id);            
            $access->delete();    
            
        }

	}

	public function accesos($iduser,$url) {
        
        $select = \DB::table('modules')
        ->select('access.views', 
            'access.inserts', 
            'access.modifys', 
            'access.deletes')
        ->join('access', function($join){
              $join->on('access.id_module', '=', 'modules.id');
        });

        $select->where('access.id_user', '=',$iduser)
        ->where('modules.url', '=',$url)
        ->where('modules.status', '=','1');

        
        $data = $select->orderBy('modules.id', 'asc')->get();

        return $data;
    }


    public function get_modules_principal_user($iduser){
		$module_principal = \DB::table('modules')
		        ->select('modules.id', 
		        'modules.description', 
		        'modules.order', 
		        'modules.id_father', 
		        'modules.url', 
		        'modules.messages', 
		        'modules.status', 
		        'modules.visible',
		        'access.views', 
		        'access.inserts', 
		        'access.modifys', 
		        'access.deletes')
		        ->join('access', function($join) use($iduser) {
		              $join->on('access.id_module', '=', 'modules.id')->where('access.id_user', '=',$iduser)
		              ->where('modules.url', '=','#');
		        })
				->where('access.id_user', '=',$iduser)
				->where('modules.status', '=','1')
				->where('modules.visible', '=','1')
		        ->orderBy('modules.order')->orderBy('modules.id')->get(); 

		return  Collection::make($module_principal);
    }

    public function get_modules_menu_user($iduser){
    	$module_menu = \DB::table('modules')
        ->select('modules.id', 
            'modules.description', 
            'modules.order', 
            'modules.id_father', 
            'modules.url', 
            'modules.messages', 
            'modules.status', 
            'modules.visible',
            'access.views', 
            'access.inserts', 
            'access.modifys', 
            'access.deletes')
        ->join('access', function($join) use($iduser) {
              $join->on('access.id_module', '=', 'modules.id')->where('access.id_user', '=',$iduser)
              ->where('modules.url', '<>','#');
        })
		->where('access.id_user', '=',$iduser)
		->where('modules.status', '=','1')
		->where('modules.visible', '=','1')
        ->orderBy('modules.order')->get();   

        return Collection::make($module_menu);
    }
}
