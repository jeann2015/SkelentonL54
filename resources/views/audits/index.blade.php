@extends('layouts.header')
@section('content')
    <table class="table table-striped">
        <tr>
            <td colspan="9">
                Auditorias
            </td>
        </tr>
        <tr>
            <td colspan="9">
                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
            </td>
        </tr>
        <tr class="success">
            <td>Id</td>
            <td>Usuario</td>
            <td>Descripción</td>
            <td>Fecha/Hora</td>
        </tr>
        @foreach ($audits as $audit)
            <tr>
                <td>{{ $audit->id }}</td>
                <td>{{ $audit->name }}</td>
                <td>{{ $audit->description }}</td>
                <td>{{ Carbon\Carbon::parse($audit->created_at)->format('l jS \\of F Y h:i:s A')  }}</td>
            </tr>
        @endforeach
    </table>
    {{ $audits->links() }}
@endsection