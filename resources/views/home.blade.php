@extends('layouts.home')

@section('content')

<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">{{ config('app.name', 'Laravel') }}</a>
        </div>

      <ul class="nav navbar-nav">
        @foreach ($module_principals as $module_principal)
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $module_principal->description }} <span class="caret"></span></a>
            <ul class="dropdown-menu">
            @foreach ($module_menus as $module_menu)
                @if ($module_menu->id_father == $module_principal->id && $module_menu->visible==1 && $module_menu->views==1)
                    <li><a href="{{ $module_menu->url }}">{{ $module_menu->description }}</a></li>
                @endif
            @endforeach
             </ul>
          </li>
        @endforeach
      </ul>
        <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ url('/logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
        </ul>

        </div>
      </div>
<!--
<div  style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);" id="map"></div> -->

<!-- <div class="container" style="height: 100%; width: 100%; ">
  <div class="jumbotron" style="height: 100%; width: 100%; position: absolute;  top: 0px; left: 0px;" id="map">

  </div>
</div> -->

</nav>

@endsection
